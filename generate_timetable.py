"""
Module to generate time table.
Based on subject file (subject .json).
Based on teacher filer (teacher.json).
We are usign recursive approch to assign teachers and subject to respective sections.
"""

import json


def generate():
    # generate the structure of timetable.json using teacher.json, subject.json 
    
    subject_data_f=open('subject.json', "r")
    teacher_data_f=open('teacher.json', "r")
    timetable_data_f=open('timetable.json', "r+")
    subject_data= json.loads(subject_data_f.read())
    teacher_data= json.loads(teacher_data_f.read())

    data = subject_data['data']
    teach_data=teacher_data['data']
    no_of_teacher=11
    result_data={"data":[{"Section 1":[]},{"Section 2":[]},{"Section 3":[]},{"Section 4":[]},{"Section 5":[]}]}
    
   
    first_period="08:00AM-09:00AM"
    second_period="09:00AM-10:00AM"
    third_period="11:00AM-12:00PM"
    fourth_period="12:00PM-01:00PM"
    
    days=['MON','TUE','WED','THU','FRI']
    periods=[first_period,second_period,third_period,fourth_period]
    #["mon":{"time":[{"sub":subjectid}]}]
    
    twenty_percentage=2
    #{"1": "t1"},{"6": "teacher6"},{"11": "teacher11"}
    twenty_percentage_teacher=[teach_data[0],teach_data[5],teach_data[10]]
    #for i in range(twenty_percentage):
    #    twenty_percentage_teacher.append(teach_data[i+5*i])

    displacement_variable=0
    displacement_variable1=0
    
    #section1
    section_1_teacherlist=[]
    section_1_sub_order=[]
    
    check_count=0
    count=0
    while(count<5):
        if(teach_data[displacement_variable] in twenty_percentage_teacher and check_count>=1):
            displacement_variable+=1
        section_1_sub_order.append(data[displacement_variable1])
        section_1_teacherlist.append(teach_data[displacement_variable])
        displacement_variable1+=1
        if(teach_data[displacement_variable] in twenty_percentage_teacher):
            check_count+=1
        if(displacement_variable1>4):
            displacement_variable1=0
        displacement_variable+=1
        if(displacement_variable>11):
            displacement_variable=0
        count+=1
    
    skip=0
    temp_val=0
    sub_count=[0,0,0,0,0]
    max_var=4
    check=0

    for i in days:
        temp_ans={i:[{}]}
        temp_count=0
        for j in periods:
            if(sub_count[temp_val]==4):
                max_var-=1
                val=sub_count.index(4)
                section_1_teacherlist.pop(val)
                section_1_sub_order.pop(val)
                sub_count.pop(val)
            if(skip==1):
                temp_ans[i][0][j]=[section_1_sub_order[temp_val],section_1_teacherlist[temp_val]]
                sub_count[temp_val]+=1
                check=section_1_teacherlist[temp_val]
                temp_val+=1
                skip=0
            else:
                if(section_1_teacherlist[temp_val] in twenty_percentage_teacher):

                    skip=1
                    temp_ans[i][0][j]=[section_1_sub_order[temp_val],section_1_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_1_teacherlist[temp_val]
                else:
                    temp_ans[i][0][j]=[section_1_sub_order[temp_val],section_1_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_1_teacherlist[temp_val]
                    temp_val+=1
            if(temp_val>max_var):
                temp_val=0

        result_data['data'][0]['Section 1'].append(temp_ans)  
        temp_count+=1

    #section2
    displacement_variable1=1
    section_2_teacherlist=[]
    section_2_sub_order=[]
    count=0
    check_count=0
    while(count<5):
        if(teach_data[displacement_variable] in twenty_percentage_teacher and check_count>=1):
            displacement_variable+=1
        section_2_sub_order.append(data[displacement_variable1])
        section_2_teacherlist.append(teach_data[displacement_variable])
        displacement_variable1+=1
        if(teach_data[displacement_variable] in twenty_percentage_teacher):
            check_count+=1
        if(displacement_variable1>4):
            displacement_variable1=0
        displacement_variable+=1
        if(displacement_variable>11):
            displacement_variable=0
        count+=1

    skip=0
    temp_val=0
    sub_count=[0,0,0,0,0]
    max_var=4
    check=0
    for i in days:
        temp_ans={i:[{}]}
        temp_count=0
        for j in periods:
            if(sub_count[temp_val]==4):
                max_var-=1
                val=sub_count.index(4)
                section_2_teacherlist.pop(val)
                section_2_sub_order.pop(val)
                sub_count.pop(val)
            if(skip==1):
                temp_ans[i][0][j]=[section_2_sub_order[temp_val],section_2_teacherlist[temp_val]]
                sub_count[temp_val]+=1
                check=section_2_teacherlist[temp_val]
                temp_val+=1
                skip=0
            else:
                if(section_2_teacherlist[temp_val] in twenty_percentage_teacher):
                    skip=1
                    temp_ans[i][0][j]=[section_2_sub_order[temp_val],section_2_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_2_teacherlist[temp_val]
                else:
                    temp_ans[i][0][j]=[section_2_sub_order[temp_val],section_2_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_2_teacherlist[temp_val]
                    temp_val+=1
            if(temp_val>max_var):
                temp_val=0
        result_data['data'][1]['Section 2'].append(temp_ans)  
        temp_count+=1 

    #section3
    displacement_variable1=2
    section_3_teacherlist=[]
    section_3_sub_order=[]
    count=0
    check_count=0
    while(count<5):
        if(teach_data[displacement_variable] in twenty_percentage_teacher and check_count>=1):
            displacement_variable+=1
        section_3_sub_order.append(data[displacement_variable1])
        section_3_teacherlist.append(teach_data[displacement_variable])
        if(teach_data[displacement_variable] in twenty_percentage_teacher):
            check_count+=1
        displacement_variable1+=1
        if(displacement_variable1>4):
            displacement_variable1=0
        displacement_variable+=1
        if(displacement_variable>11):
            displacement_variable=0
        count+=1
    

    skip=0
    temp_val=0
    sub_count=[0,0,0,0,0]
    max_var=4
    check=0
    for i in days:
        temp_ans={i:[{}]}
        temp_count=0
        
        for j in periods:
            if(sub_count[temp_val]==4):
                max_var-=1
                val=sub_count.index(4)
                section_3_teacherlist.pop(val)
                section_3_sub_order.pop(val)
                sub_count.pop(val)
            if(skip==1):
                temp_ans[i][0][j]=[section_3_sub_order[temp_val],section_3_teacherlist[temp_val]]
                sub_count[temp_val]+=1
                check=section_3_teacherlist[temp_val]
                temp_val+=1
                skip=0
            else:
                if(section_3_teacherlist[temp_val] in twenty_percentage_teacher):
                    skip=1
                    temp_ans[i][0][j]=[section_3_sub_order[temp_val],section_3_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_3_teacherlist[temp_val]
                else:
                    temp_ans[i][0][j]=[section_3_sub_order[temp_val],section_3_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_3_teacherlist[temp_val]
                    temp_val+=1
            if(temp_val>max_var):
                temp_val=0
        result_data['data'][2]['Section 3'].append(temp_ans)  
        temp_count+=1 


    #section4
    displacement_variable1=3
    section_4_teacherlist=[]
    section_4_sub_order=[]
    count=0
    check_count=0
    while(count<5):
        if(teach_data[displacement_variable] in twenty_percentage_teacher and check_count>=1):
            displacement_variable+=1
        section_4_sub_order.append(data[displacement_variable1])
        section_4_teacherlist.append(teach_data[displacement_variable])
        displacement_variable1+=1
        if(teach_data[displacement_variable] in twenty_percentage_teacher):
            check_count+=1
        if(displacement_variable1>4):
            displacement_variable1=0
        displacement_variable+=1
        if(displacement_variable>11):
            displacement_variable=0
        count+=1
    
    skip=0
    temp_val=0
    sub_count=[0,0,0,0,0]
    max_var=4
    check=0
    for i in days:
        temp_ans={i:[{}]}
        temp_count=0
        
        for j in periods:
            if(sub_count[temp_val]==4):
                max_var-=1
                val=sub_count.index(4)
                section_4_teacherlist.pop(val)
                section_4_sub_order.pop(val)
                sub_count.pop(val)
            if(i=='MON' and j==second_period):
                temp_val+=1
                if(temp_val>max_var):
                    temp_val=0
            
            if(i=='TUE' and j==second_period):
                temp_val+=1
                if(temp_val>max_var):
                    temp_val=0

            if(skip==1):
                temp_ans[i][0][j]=[section_4_sub_order[temp_val],section_4_teacherlist[temp_val]]
                sub_count[temp_val]+=1
                check=section_4_teacherlist[temp_val]
                temp_val+=1
                skip=0
            else:
                if(section_4_teacherlist[temp_val] in twenty_percentage_teacher):
                    skip=1
                    temp_ans[i][0][j]=[section_4_sub_order[temp_val],section_4_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_4_teacherlist[temp_val]
                else:
                    temp_ans[i][0][j]=[section_4_sub_order[temp_val],section_4_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_4_teacherlist[temp_val]
                    temp_val+=1
            if(temp_val>max_var):
                temp_val=0
        result_data['data'][3]['Section 4'].append(temp_ans)  
        temp_count+=1 


    #section5
    displacement_variable1=4
    section_5_teacherlist=[]
    section_5_sub_order=[]
    count=0
    while(count<5):
        if(teach_data[displacement_variable] in twenty_percentage_teacher):
            displacement_variable+=1
            if(displacement_variable>11):
                displacement_variable=0
        section_5_sub_order.append(data[displacement_variable1])
        section_5_teacherlist.append(teach_data[displacement_variable])
        displacement_variable1+=1
        if(displacement_variable1>4):
            displacement_variable1=0
        displacement_variable+=1
        if(displacement_variable>11):
            displacement_variable=0
        count+=1

    skip=0
    temp_val=0
    sub_count=[0,0,0,0,0]
    max_var=4
    check=0
    for i in days:
        temp_ans={i:[{}]}
        temp_count=0
        for j in periods:
            if(sub_count[temp_val]==4):
                max_var-=1
                val=sub_count.index(4)
                section_5_teacherlist.pop(val)
                section_5_sub_order.pop(val)
                sub_count.pop(val)

            if(i=='MON' and j==third_period):
                temp_val+=1
                if(temp_val>max_var):
                    temp_val=0
            if(skip==1):
                temp_ans[i][0][j]=[section_5_sub_order[temp_val],section_5_teacherlist[temp_val]]
                sub_count[temp_val]+=1
                check=section_5_teacherlist[temp_val]
                temp_val+=1
                skip=0
            else:
                if(section_5_teacherlist[temp_val] in twenty_percentage_teacher):
                    skip=1
                    temp_ans[i][0][j]=[section_5_sub_order[temp_val],section_5_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_5_teacherlist[temp_val]
                else:
                    temp_ans[i][0][j]=[section_5_sub_order[temp_val],section_5_teacherlist[temp_val]]
                    sub_count[temp_val]+=1
                    check=section_5_teacherlist[temp_val]
                    temp_val+=1
            if(temp_val>max_var):
                temp_val=0
        result_data['data'][4]['Section 5'].append(temp_ans)  
        temp_count+=1 

    ans=result_data['data'][4]['Section 5'][3]['THU'][0][third_period]
    result_data['data'][4]['Section 5'][4]['FRI'][0][first_period]=[section_5_sub_order[3],section_5_teacherlist[3]]
    result_data['data'][4]['Section 5'][4]['FRI'][0][second_period]=ans
    result_data['data'][4]['Section 5'][4]['FRI'][0][third_period]=[section_5_sub_order[1],section_5_teacherlist[1]]
    result_data['data'][4]['Section 5'][4]['FRI'][0][fourth_period]=[section_5_sub_order[2],section_5_teacherlist[2]]
    result_data['data'][4]['Section 5'][3]['THU'][0][third_period]=[section_5_sub_order[1],section_5_teacherlist[1]]
  
    timetable_data_f.seek(0)
    json.dump(result_data,timetable_data_f)
    print("Done..!\n")
    timetable_data_f.close()
    subject_data_f.close()
    teacher_data_f.close()


def section_generate(section_number):
    # Display the section wise time table
    try:
        timetable_data_f=open('timetable.json', "r+")
        timetable_data= json.loads(timetable_data_f.read())
        timetable=timetable_data['data']

        temp_Val=["Section 1","Section 2","Section 3","Section 4","Section 5"]
        days=['MON','TUE','WED','THU','FRI']
        first_period="08:00AM-09:00AM"
        second_period="09:00AM-10:00AM"
        third_period="11:00AM-12:00PM"
        fourth_period="12:00PM-01:00PM"

        periods=[first_period,second_period,third_period,fourth_period]
        temp_data=timetable[section_number-1][temp_Val[section_number-1]]

        print("\nTIME TABLE OF SECTION ",str(section_number))
        print()
        print_var=[["Day",first_period,second_period,third_period,fourth_period]]
        for i in range(5):
            disp_temp=[days[i]]
            temp=0
            for j in periods:
                disp_temp.append(list(temp_data[i][days[i]][temp][j][0].values())[0]+"("+list(temp_data[i][days[i]][temp][j][1].values())[0]+")")
            print_var.append(disp_temp)
    
        for row in print_var:
            day , first, second, third, fourth = row
            print ("{:<8} {:<28} {:<28} {:<28} {:<28}".format(day,first,second,third,fourth))
        timetable_data_f.close()
    except:
        print("Generate the timetable first!\n")

def teacher_generate(teacher_id):
    # Display the time table of a particular teacher.
    try:
        timetable_data_f=open('timetable.json', "r+")
        timetable_data= json.loads(timetable_data_f.read())
        timetable=timetable_data['data']
        
        temp_Val=["Section 1","Section 2","Section 3","Section 4","Section 5"]
        days=['MON','TUE','WED','THU','FRI']
        first_period="08:00AM-09:00AM"
        second_period="09:00AM-10:00AM"
        third_period="11:00AM-12:00PM"
        fourth_period="12:00PM-01:00PM"

        periods=[first_period,second_period,third_period,fourth_period]


        print_var=[]
    
        for i in range(5):
            for row in (timetable[i][temp_Val[i]]):
                temp_var=[list(row.keys())[0]]
                for each_element in (list(row.values())[0]):
                    for iter_val in periods:
                        if(int(list(each_element[iter_val][1].keys())[0])==teacher_id):
                            temp_var.append(list(each_element[iter_val][0].values())[0])
                        else:
                            temp_var.append("NULL")
                print_var.append(temp_var)
    
        print_var2=[["Day",first_period,second_period,third_period,fourth_period]]

        for i in range(5):
            iter_val=(i)
            section=1
            new_row=[print_var[iter_val][0],"NULL","NULL","NULL","NULL"]
            while iter_val<25:
                #print(print_var[iter_val])
                for row in range(1,5):
                    if(new_row[row]=="NULL" and print_var[iter_val][row]!="NULL"):
                        ans=""+str(print_var[iter_val][row])+"(Section-"+str(section)+")"
                        new_row[row]=ans
                iter_val+=5
                section+=1
                if(section>5):
                    section=1

            print_var2.append(new_row)

 
        print("\nTIME TABLE OF Teacher ",str(teacher_id))
        print()
        for row in print_var2:
            day , first, second, third, fourth = row
            print ("{:<8} {:<28} {:<28} {:<28} {:<28}".format(day,first,second,third,fourth))
        timetable_data_f.close()
    except:
        print("Generate the timetable first!\n")
