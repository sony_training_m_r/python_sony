"""
This is the main module (User Interface)
"""

import json
import store_data
import generate_timetable

def add_subject_data(subject_name):
    # add subject data calling store_data module
    store_data.store_subject(subject_name)

def add_teacher_data(teacher_name):
    # add teacher data calling store_data module
    store_data.store_teacher(teacher_name)

def display_teacherwise(teacher_id):
    # display teacher time table using generate_timetable module
    generate_timetable.teacher_generate(teacher_id)

def display_sectionwise(section_id):
    # display section time table using generate_timetable module6
    generate_timetable.section_generate(section_id)

def check_warning_condition():
    # checks warning condition with respect to subject and teacher constrains.
    subject_data_f=open('subject.json', "r")
    teacher_data_f=open('teacher.json', "r")
    subject_data= json.loads(subject_data_f.read())
    teacher_data= json.loads(teacher_data_f.read())
    
    sub_count=len(subject_data['data'])
    teacher_count=len(teacher_data['data'])
    if(sub_count<4 or teacher_count<11):
        return 1
    else:
        return 0

LoopCondition = 1
while(LoopCondition):
    
    #import the files (Since updated file is required its inside the for loop)
 
    subject_data_f=open('subject.json', "r")
    teacher_data_f=open('teacher.json', "r")
    subject_data= json.loads(subject_data_f.read())
    teacher_data= json.loads(teacher_data_f.read())


    print("Welcome to Time Table Management System\n")
    if(check_warning_condition()):
        print("WARNING INSUFFICIENT DATA\n")
    print("Choose your option")
    print("1] Add Subject Details \t2] Add Teacher Details \t3] Generate Time Table \t4] Display Teacher Time Table \t5] Display Section Time Table \t6] Exit \n")
    
    choice=int(input())

    if(choice==1):
        sub_count=len(subject_data['data'])
        if(sub_count>4):
            print("Reached Maximum Subject Count\n")
        else:
            sub_name=input("Enter the subject name or Enter for default name ==> ")
            add_subject_data(sub_name)

    elif(choice==2):
        
        teach_count=len(teacher_data['data'])
        if(teach_count>11):
            print("Reached Maximum Teacher Count\n")
        else:
            teacher_name=input("Enter the Teacher name or Enter for default name ==> ")
            add_teacher_data(teacher_name)
        
    elif(choice==3):
        if(check_warning_condition()):
            print("INSUFFICIENT DATA CAN'T GENERATE TIME TABLE\n")
        else:
            generate_timetable.generate()
    elif(choice==4):
        print("Enter the Teachers ID:\n")
        teach_id=int(input())
        teach_count=len(teacher_data['data'])
        if(teach_id<=0 or teach_id>teach_count):
            print("Invalid ID")
        else:
            display_teacherwise(teach_id)
    elif(choice==5):
        print("Enter the Section Number (1-5):\n")
        section_input=int(input())
        if(section_input<=0 or section_input>5):
            print("Invalid Section Number")
        else:
            display_sectionwise(section_input)
    elif(choice==6):
        LoopCondition = 0
    else:
        print("\nINVALID INPUT!!")
